<?php

namespace Database\Factories;

use App\Models\Category;
use App\Models\Post;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Post>
 */
class PostFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'image' => fake()->imageUrl(),
            'title' => fake()->sentence(5),
            'content' => fake()->paragraph(),
            'category_id' => Category::query()->get()->random()->id,
            'likes' => rand(1, 100),
            'published' => true,
        ];
    }
}
