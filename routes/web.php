<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PostController;
use App\Http\Controllers\AboutController;
use App\Http\Controllers\MainController;
use App\Http\Controllers\ContactController;

use App\Http\Controllers\Post\IndexController;
use App\Http\Controllers\Post\CreateController;
use App\Http\Controllers\Post\StoreController;
use App\Http\Controllers\Post\ShowController;
use App\Http\Controllers\Post\EditController;
use App\Http\Controllers\Post\UpdateController;
use App\Http\Controllers\Post\DestroyController;
use App\Http\Controllers\Admin\Post\AdminController;
use \App\Http\Controllers\HomeController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', [HomeController::class, 'index'])->name('home');

//Route::get('/posts/createOrFake', [PostController::class, 'createOrFake'])->name('posts.createOrFake');

Route::group(['namespace' => 'App\Http\Controllers\Post'], function () {

    Route::get('/posts', IndexController::class)->name('posts.index');

    Route::get('/posts/create', CreateController::class)->name('posts.create');

    Route::post('/posts', StoreController::class)->name('posts.store');

    Route::get('posts/{post}', ShowController::class)->name('posts.show');

    Route::get('/posts/{post}/edit', EditController::class)->name('posts.edit');

    Route::patch('/posts/{post}', UpdateController::class)->name('posts.update');

    Route::delete('/posts/{post}', DestroyController::class)->name('posts.destroy');

});


Route::group(['namespace' => 'App\Http\Controllers\Admin', 'prefix' => 'admin', 'middleware' => 'admin'], function () {
    Route::group(['namespace' => 'Post'], function () {
        Route::get('/posts', AdminController::class)->name('admin.post.index');

    });
});


//Route::get('/posts/update', [PostController::class, 'update'])->name('posts.update');
//
//Route::get('/posts/first_or_create', [PostController::class, 'firstOrCreate'])->name('posts.firstOrCreate');
//
//Route::get('/posts/update_or_create', [PostController::class, 'updateOrCreate'])->name('posts.updateOrCreate');
//
//Route::get('/posts/{id}', [PostController::class, 'destroy'])->name('posts.destroy');


Route::get('/about', [AboutController::class, 'index'])->name('about.index');
Route::get('/contact', [ContactController::class, 'index'])->name('contact.index');
Route::get('/main', [MainController::class, 'index'])->name('main.index');


Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
