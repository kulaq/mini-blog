@extends('layouts.main')


@section('content')

    <div class="container mb-3">
        {{ $post->id }}. {{ $post->title }}
        <br>
        <hr>
        {{ $post->content }}
    </div>

    <div class="container">
        <a class="btn btn-outline-primary mb-3" href="{{ route('posts.edit', $post->id) }}">
            edit
        </a>
    </div>

    <div class="container">
        <form action="{{ route('posts.destroy', $post->id) }}" method="post">
            @csrf
            @method('delete')
            <input type="submit" value="del" class="btn btn-outline-danger mb-3">
        </form>
    </div>

    <div class="container">
        <a href="{{ route('posts.index') }}">
            Back
        </a>
    </div>

@endsection
