@extends('layouts.main')


@section('content')
    <div class="container">
        <form action="{{ route('posts.update', $post->id) }}" method="post">
            @csrf
            @method('patch')
            <div class="mb-3">
                <label for="image" class="form-label">Image</label>
                <input type="text" name="image" class="form-control" id="image"  value="{{ $post->image }}" placeholder="Image">
            </div>


            <div class="mb-3">
                <label for="title" class="form-label">Title</label>
                <input type="text" name="title" class="form-control" id="title"  value="{{ $post->title }}" placeholder="Title">
            </div>


            <div class="mb-3">
                <label for="content" class="form-label">Content</label>
                <textarea name="content" class="form-control" id="content"  placeholder="Content">{{ $post->content }}</textarea>
            </div>

            <div class="form-group mb-3">
                <label for="category">Category</label>
                <select class="form-control" id="category" name="category_id">
                    @foreach($categories as $category)
                        <option
                            {{ $category->id === $post->category_id ? 'selected' : ''}}
                            value="{{ $category->id }}">{{ $category->title }}</option>
                    @endforeach
                </select>
            </div>

            <div class="form-group mb-3">
                <label for="tags">Tags</label>
                <select class="form-control" multiple id="tags" name="tags[]">
                    @foreach($tags as $tag)
                        <option
                            @foreach($post->tags as $postTags)
                            {{ $tag->id === $postTags->id ? 'selected' : ''}}
                            @endforeach
                            value="{{ $tag->id }}">{{ $tag->title }}</option>
                    @endforeach
                </select>
            </div>


            <button type="submit" class="btn btn-outline-danger">Update</button>
        </form>
    </div>
<br>
    <div class="container">
        <a href="{{ route('posts.show', $post->id) }}">
            Назад
        </a>
    </div>
@endsection
