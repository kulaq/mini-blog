@extends('layouts.admin')

@section('content')
    <nav class="navbar bg-body-tertiary">
        <div class="container mb-3">
            <form action="{{ route('posts.index') }}" class="d-flex" method="get">
                <input
                    value="{{ request('search') }}" class="form-control me-2"
                    name="search" placeholder="{{ __('Search') }}" aria-label="Поиск">
                <button class="btn btn-outline-success" type="submit">Поиск</button>
            </form>

            <form class="d-flex" action="{{ route('posts.create') }}">
                <button class="btn btn-danger">New Post</button>
            </form>
        </div>

    </nav>



    <div class="container">
        @foreach($posts as $post)
            <div><a href="{{ route('posts.show', $post->id) }}">{{ $post->id }}.
                    {{ $post->title }}</a></div>
            <br>
        @endforeach
        <hr>
    </div>

    <div class="container mb-3">
        {{ $posts->withQueryString()->links() }}
    </div>
@endsection
