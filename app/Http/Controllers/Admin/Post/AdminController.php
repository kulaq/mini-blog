<?php

namespace App\Http\Controllers\Admin\Post;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Post\BaseController;
use App\Models\Post;
use Illuminate\Http\Request;
use App\Filters\PostFilter;

class AdminController extends Controller
{
    public function __invoke(PostFilter $request)
    {
        $posts = Post::filter($request)->paginate(10);

        return view('admin.posts.index', compact('posts'));
    }
}
