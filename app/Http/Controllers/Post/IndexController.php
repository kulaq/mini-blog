<?php

namespace App\Http\Controllers\Post;

use App\Http\Controllers\Controller;
use App\Models\Post;
use Illuminate\Http\Request;
use App\Filters\PostFilter;

class IndexController extends BaseController
{
    public function __invoke(PostFilter $request)
    {
        $posts = Post::filter($request)->paginate(10);

//        return response()->json($posts, 201);
        return view('posts.index', compact('posts'));
    }
}
