<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Post;
use App\Models\PostTag;
use App\Models\Tag;
use Illuminate\Http\Request;

class PostController extends Controller
{
    public function index()
    {
        $posts = Post::all();
        return view('posts.index', compact('posts'));
    }

    public function create()
    {
        $categories = Category::all();
        $tags = Tag::all();

        return view('posts.create', compact('categories', 'tags'));
    }

    public function store(Request $request)
    {
        $validated = $request->validate([
            'image' => ['string'],
            'title' => ['required', 'string'],
            'content' => ['string'],
            'category_id' => [''],
            'tags' => ['required'],
        ]);
        $tags = $validated['tags'];
        unset($validated['tags']);


        $post = Post::query()->create($validated)
            ->tags()
            ->attach($tags, [
                'created_at' => new \DateTime('now'),
                'updated_at' => new \DateTime('now')
            ]);

        return redirect()->route('posts.index');
    }

    public function show(Request $request, Post $post)
    {
        return view('posts.show', compact('post'));
    }


    public function edit(Post $post)
    {
        $categories = Category::all();
        $tags = Tag::all();

        return view('posts.edit', compact('post', 'categories', 'tags'));
    }

    public function update(Request $request, Post $post)
    {
        $validated = $request->validate([
            'image' => ['string'],
            'title' => ['string'],
            'content' => ['string'],
            'category_id' => [''],
            'tags' => [''],
        ]);

        $tags = $validated['tags'];
        unset($validated['tags']);

        $post->update($validated);
        $post->tags()->sync($tags);

        return redirect()->route('posts.show', $post->id);

    }

    public function destroy(Post $post)
    {
        $post->delete();

        return redirect()->route('posts.index');
    }


    public function firstOrCreate()
    {
        $post = Post::query()->firstOrCreate([
            "id" => 1,
        ], [
            "created_at" => "2024-03-14T08:21:59.000000Z",
            "updated_at" => "2024-03-14T08:21:59.000000Z",
            "image" => "640x480.png",
            "title" => "Laudantium debitis",
            "content" => "Excepturi deserunt aut non nobis repellendus ut.",
            "likes" => 6,
            "published" => true,
            "deleted_at" => null,
        ]);

        dd($post->toArray());

    }

    public function updateOrCreate()
    {
        $post = Post::query()->updateOrCreate([
            "id" => 6,
        ], [
            "created_at" => "2024-03-14T08:21:59.000000Z",
            "updated_at" => "2024-03-14T08:21:59.000000Z",
            "image" => "CREATE",
            "title" => "CREATE",
            "content" => "CREATE Excepturi deserunt aut non nobis repellendus ut.",
            "likes" => 6,
            "published" => true,
            "deleted_at" => null,
        ]);

        dd($post);

    }

    public function createOrFake()
    {
        $animal = ['cats', 'dogs'];

        for ($i = 0; $i < 3; $i++) {
            $post = Post::query()->create([
                'image' => fake()->imageUrl(),
                'title' => fake()->sentence(),
                'content' => fake()->paragraph(),
                'likes' => rand(1, 20),
                'published' => true,
            ]);
        }

        dd('success');
    }
}
