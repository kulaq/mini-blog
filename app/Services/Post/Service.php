<?php

namespace App\Services\Post;

use App\Models\Post;

class Service
{

    public function store($validated)
    {
        $tags = $validated['tags'];
        unset($validated['tags']);


        $post = Post::query()->create($validated)
            ->tags()
            ->attach($tags, [
                'created_at' => new \DateTime('now'),
                'updated_at' => new \DateTime('now')
            ]);
    }


    public function update($post, $validated)
    {

        $tags = $validated['tags'];
        unset($validated['tags']);

        $post->update($validated);
        $post->tags()->sync($tags);

    }
}
