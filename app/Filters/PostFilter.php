<?php

namespace App\Filters;

class PostFilter extends QueryFilter
{
    public function search($search = '')
    {
        return $this->builder->where('title', 'LIKE', '%'.$search.'%');
    }
}
